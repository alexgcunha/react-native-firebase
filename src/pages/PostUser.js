import React, { Component } from 'react';
import { View, Text,TextInput, TouchableOpacity, StyleSheet,AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';

export default class PostsUser extends Component {
    static navigationOptions = { header: null }
state = {
    saudeGeo: '',
    posts: '',
    userRef:'',
        
};



Posts = async () =>{
  const author =  await AsyncStorage.getItem('@db:nome')
    const url = await AsyncStorage.getItem('@db:imagem')
    const _id = firebase.auth().currentUser.uid;
    const {posts, saudeGeo} = this.state

   //PostKey = firebase.database.ref('posts').push().key
    //console.log(newPostKey);
   // firebase()
   await  firebase.database().ref('posts').push({
    saudeGeo,
    posts,
    author,
    url,
    _id
   });

await  firebase.database().ref('posts-user/'+ _id).push({
    saudeGeo,
    posts,
    author,
    url,
    _id
   })



   
   this.props.navigation.pop();
}
goBack = () => {this.props.navigation.pop();

}

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Icon name="close" size={24} color="#ff944d"/>
                    </TouchableOpacity >
                        
                    <TouchableOpacity style={styles.button} onPress={this.Posts}> 
          
                    <Text style={styles.buttonText}>Postar</Text>

                    </TouchableOpacity>
          
                    </View>
                    <TextInput 
                            style={styles.inputGeo}
                            placeholder="Posto da sacramenta"
                            value={this.state.saudeGeo}
                            onChangeText={saudeGeo => this.setState({ saudeGeo })}
                            placeholderTextColor="#333"
                        
                        />
 

                  <TextInput 
                    style={styles.input}
                    multline={true}
                    placeholder="O que esta acontecendo  "
                    value={this.state.posts}
                    onChangeText={posts => this.setState({ posts })}
                    placeholderTextColor="#333"
                    returnKeyType="send"
                    />
                    
               </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#cccccc"
  },

  header: {
    paddingTop: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },

  button: {
    height: 32,
    paddingHorizontal: 20,
    borderRadius: 16,
    backgroundColor: "#ff944d",
    justifyContent: "center",
    alignItems: "center"
  },

  buttonText: {
    color: "#FFF",
    fontSize: 16,
    fontWeight: "bold"
  },

  input: {
    //margin: 20,
    fontSize: 16,
    color: "#333",
    borderWidth: 1,
    borderColor: "#333",
    borderRadius: 5,
    height: 44,
    paddingHorizontal: 15,
    alignSelf: "stretch",
    marginTop: 30
  },
  inputGeo: {
    borderWidth: 1,
    borderColor: "#333",
    borderRadius: 5,
    height: 44,
    paddingHorizontal: 15,
    alignSelf: "stretch",
    marginTop: 30
  },
});