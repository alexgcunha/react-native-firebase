import React, { Component } from 'react';
import { View, Text,TextInput, TouchableOpacity, StyleSheet,AsyncStorage, KeyboardAvoidingView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';

export default class PostagemVacina extends Component {
    static navigationOptions = { header: null }
state = {
    saudeGeo: '',
    vacina: '',
    quantidade:'',
    descritions: '',
    
        
};



Posts = async () =>{
  const author =  await AsyncStorage.getItem('@db:nome')
    const url = await AsyncStorage.getItem('@db:imagem')
    const _id = firebase.auth().currentUser.uid;
    const {posts, saudeGeo, vacina,quantidade} = this.state

   await  firebase.database().ref('posts-vacina').push({
    saudeGeo,
    vacina,
    quantidade,
    author,
    url,
    _id
   });

 this.props.navigation.pop();
}
goBack = () => {this.props.navigation.pop();

}

    render(){
        return(
            <KeyboardAvoidingView style={styles.container} behavior="height">
                <View style={styles.header}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Icon name="close" size={24} color="#ff944d"/>
                    </TouchableOpacity >
                        
                    <TouchableOpacity style={styles.button} onPress={this.Posts}> 
          
                    <Text style={styles.buttonText}>Postar</Text>

                    </TouchableOpacity>
          
                    </View>
                
                
                    <TextInput 
                            style={styles.input}
                            placeholder="Informe o local"
                            value={this.state.saudeGeo}
                            onChangeText={saudeGeo => this.setState({ saudeGeo })}
                            placeholderTextColor="#333"
                        
                        />
 

                  <TextInput 
                    style={styles.input}
                    multline={true}
                    placeholder=" Informe o tipo da vacina "
                    value={this.state.vacina}
                    onChangeText={vacina => this.setState({ vacina })}
                    placeholderTextColor="#333"
                    returnKeyType="send"
                    />

                    <TextInput 
                            style={styles.input}
                            placeholder="informe a quantidade disponivel"
                            value={this.state.quantidade}
                            onChangeText={quantidade => this.setState({ quantidade })}
                            placeholderTextColor="#333"
                        
                    />
                    
               </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#cccccc"
  },

  header: {
    paddingTop: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },

  button: {
    height: 32,
    paddingHorizontal: 20,
    borderRadius: 16,
    backgroundColor: "#ff944d",
    justifyContent: "center",
    alignItems: "center"
  },

  buttonText: {
    color: "#FFF",
    fontSize: 16,
    fontWeight: "bold"
  },

  input: {
    margin: 20,
    fontSize: 16,
    color: "#333",
    borderWidth: 1,
    borderColor: "#333",
    borderRadius: 5,
    height: 44,
    paddingHorizontal: 15,
    alignSelf: "stretch",
    marginTop: 15   
  },
  inputGeo: {
    borderWidth: 1,
    borderColor: "#333",
    borderRadius: 5,
    height: 44,
    paddingHorizontal: 15,
    alignSelf: "stretch",
    marginTop: 30
  },
  
});