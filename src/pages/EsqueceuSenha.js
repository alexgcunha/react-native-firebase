import React, { Component } from 'react';
import { View, KeyboardAvoidingView ,Text, TextInput, TouchableOpacity ,StyleSheet } from 'react-native';
import firebase from 'react-native-firebase';
import {StackActions, NavigationActions } from 'react-navigation';

export default class EsqueceuSenha extends Component{
    static navigationOptions = { header: null }
state={
    email:"",
    };

handleSenha = () => {
const { email } = this.state  
    firebase.auth().sendPasswordResetEmail(email).then(
      this.props.navigation.navigate('Login')
).catch(function(error) {
  console.log(error)
});

  }

  esqueuSenha = () =>{
    this.props.navigation.navigate('EsqueceuSenha')
  }

    render(){
        return(
            <KeyboardAvoidingView style={styles.container}> 
                <View style={styles.content}>
                <View>
                <Text style={styles.TextLogin}>Recuperar Senha</Text>
                </View>
                
                    <TextInput 
                        style={styles.input}
                        placeholder="DIGITE SEU E-MAIL"
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                    
                    />
                    
                    <TouchableOpacity style={styles.button} onPress={this.handleSenha}>
                   
                    <Text style={styles.buttonText}>Enviar</Text>

                    </TouchableOpacity>
                    
                   
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#cccccc"
    },
  
    content: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      padding: 30
    },
  
    input: {
      borderWidth: 1,
      borderColor: "#333",
      borderRadius: 5,
      height: 44,
      paddingHorizontal: 15,
      alignSelf: "stretch",
      marginTop: 30
    },
  
    button: {
      height: 44,
      alignSelf: "stretch",
      marginTop: 10,
      backgroundColor: "#ff944d",
      borderRadius: 5,
      justifyContent: "center",
      alignItems: "center"
    },
  
    buttonText: {
      color: "#FFF",
      fontSize: 16,
      fontWeight: "bold"
    },
    TextLogin: {
        
        fontSize: 20,
        fontWeight: "bold"
    }
  });
  

