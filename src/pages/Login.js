import React, { Component } from 'react';
import { View, KeyboardAvoidingView ,Text, TextInput, TouchableOpacity ,StyleSheet } from 'react-native';
import firebase from 'react-native-firebase';
import {StackActions, NavigationActions } from 'react-navigation';

export default class Login extends Component{
    static navigationOptions = { header: null }
state={
    email:"",
    password:"",
 
    errorMessage: null
};

handleSignUp = async () => {
    const { email, password } = this.state;
    try{
       await firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
       this.props.navigation.navigate('Main')
    }catch(error){
        console.log(error);
    }
  }

  esqueceuSenha = () =>{
    this.props.navigation.navigate('EsqueceuSenha')
  }

  navigateToLogin = () =>{
    const resetAction = StackAction.reset({
        index: 0,
        actions: [
            NavigationActions.navigate({ routeName: 'Main'})
        ]
       
    });
    this.props.navigation.dispatch(resetAction);
}



    render(){
        return(
            <KeyboardAvoidingView style={styles.container}> 
                <View style={styles.content}>
                <View>
                <Text style={styles.TextLogin}>Login</Text>
                </View>
                {this.state.errorMessage &&
                 <Text style={{ color: 'red' }}>
                {this.state.errorMessage}
                </Text>}
                    <TextInput 
                        style={styles.input}
                        placeholder="DIGITE SEU E-MAIL"
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                    
                    />
                    
                     <TextInput 
                        secureTextEntry
                        style={styles.input}
                        
                        placeholder="DIGITE SUA SENHA"
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })}
                    
                    />

                    <TouchableOpacity style={styles.button} onPress={this.handleSignUp}>
                    <Text style={styles.buttonText}>Entrar</Text>

                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={this.esqueceuSenha}>

                      <Text>Esqueceu a senha</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#cccccc"
    },
  
    content: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      padding: 30
    },
  
    input: {
      borderWidth: 1,
      borderColor: "#333",
      borderRadius: 5,
      height: 44,
      paddingHorizontal: 15,
      alignSelf: "stretch",
      marginTop: 30
    },
  
    button: {
      height: 44,
      alignSelf: "stretch",
      marginTop: 10,
      backgroundColor: "#ff944d",
      borderRadius: 5,
      justifyContent: "center",
      alignItems: "center"
    },
  
    buttonText: {
      color: "#FFF",
      fontSize: 16,
      fontWeight: "bold"
    },
    TextLogin: {
        
        fontSize: 20,
        fontWeight: "bold"
    }
  });
  

