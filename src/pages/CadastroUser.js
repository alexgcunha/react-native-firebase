import React,{ Component } from 'react';
import {View, Text, StyleSheet,TextInput, TouchableOpacity,  AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';

export default class CadastroUser extends Component{
    static navigationOptions = { header: null }

state = {
    uid:'',
    nome:"",
    endereco:"",
    nascimento:"",
    imagem: null
};

 
handler = async () =>{
        const { nome } = this.state;
       

        await AsyncStorage.setItem('@db:nome', nome);

        
    }


cadastrar =  () =>{
    const userId = firebase.auth().currentUser.uid;
    const { nome, endereco, nascimento, uid, imagem } = this.state;   
   
    firebase.database().ref('users').child(userId).set({
        uid: userId,
        nome,
        endereco,
        nascimento,

    }).then(this.handler).then(this.props.navigation.navigate('Main'));
 }
 


    render(){
       
        return(
            <View style={styles.container}>
           
                <View style={styles.content}>

       


                    <Text style={styles.Texto}>DADOS DO USUÁRIO</Text>
                    <TextInput 
                            style={styles.input}
                            placeholder="DIGITE SEU NOME COMPLETO"
                            value={this.state.nome}
                            onChangeText={nome => this.setState({ nome })}
                        
                        />
                    <TextInput 
                            style={styles.input}
                            placeholder="DIGITE SEU ENDERECO"
                            value={this.state.endereco}
                            onChangeText={endereco => this.setState({ endereco })}
                        
                        />
                    <TextInput 
                            style={styles.input}
                            placeholder="DIGITE ANO DE NASCIMENTO"
                            value={this.state.nascimento}
                            onChangeText={nascimento => this.setState({ nascimento })}
                        
                        />

                        
                    <TouchableOpacity style={styles.button} onPress={this.cadastrar}>
                        <Text style={styles.buttonText}>Cadastrar</Text>

                        </TouchableOpacity>

                        
                    </View>
         </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#cccccc"
    },
  
    content: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      padding: 30
    },
  
    input: {
      borderWidth: 1,
      borderColor: "#333",
      borderRadius: 5,
      height: 44,
      paddingHorizontal: 15,
      alignSelf: "stretch",
      marginTop: 30
    },
    button: {
        height: 44,
        alignSelf: "stretch",
        marginTop: 10,
        backgroundColor: "#ff944d",
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center"
      },
    
      buttonText: {
        color: "#FFF",
        fontSize: 16,
        fontWeight: "bold"
      },
      Texto: {
          
          fontSize: 20,
          fontWeight: "bold"
      }
})