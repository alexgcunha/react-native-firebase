import React, { Component } from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Button, Image, AsyncStorage, FlatList,ListView, ScrollView} from 'react-native';
import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import { ListItem, List } from 'react-native-elements'
import HeaderHome from '../components/headerHome/index';


export default class Main extends Component {
  static navigationOptions = { header: null }
  state = { currentUser: null,
    todo: [],
    imagem: null,
    chave: [],
    like: 0
    
  }
  async componentDidMount() {
    const { currentUser  } = firebase.auth()
 
    this.setState({ currentUser })
    const email = firebase.auth().currentUser.email;
    const ref = firebase.storage().ref()
           const uid = firebase.auth().currentUser.uid;
                  ref.child(email+ '/' + uid).getDownloadURL().then(url =>{
        

    
                      this.setState({ imagem: url})

      }).then(this.handler).catch(function(error){
        console.log(error)
      })

      const db = firebase.database();
      const userId = firebase.auth().currentUser.uid;
      //const key = firebase.database().ref('posts').push().key;
      //console.log('teste ' +key );
      db.ref('posts').on('value', (snapshot) => {
       
        
        let obj = snapshot.toJSON()
        let message = [];
        let t =[];
        for(let id in obj){
          message.push({...obj[id]});
          t.push({id});
        }
        this.setState({ todo: message });
        this.setState({ chave: t});
      })
    

  }

likeTeste = async() =>{
  const { chave, like} =this.state
  let c = chave.length;
  let t ;
  let v;
  
  for(x in chave){
    t = chave[x].id
  }
  
}
  


  
  handler = async () =>{
    try{
    const { imagem } = this.state;
   
    await AsyncStorage.setItem('@db:imagem', imagem);

    const teste = await AsyncStorage.getItem('@db:imagem')

    console.log( teste);
}catch(error){
  console.log(error);
}
    
}

_keyExtractor = item => item.posts + item._id;
render() {
    const { chave, todo } = this.state
    
return (
      <View style={styles.container}>
        <HeaderHome />
       
      

        <FlatList 
                data={this.state.todo}
                
               keyExtractor={this._keyExtractor }
               
                renderItem={({item, index}) => (
                    
   
                   <View style={styles.conteinerList}  key={item.posts + item._id}>
                   
                   
                   <View style={styles.ViewLis}>
                
                   <Image
                      source={{uri: item.url}}                  
                      style={styles.avatar}
                  />
                     <Text style={styles.repoNome}>{item.author}</Text>   
                  </View>
                 
                       
                        
                        <Text style={styles.contentPost}>Local:  {item.saudeGeo}</Text>                      
                        <Text style={styles.contentPost}>{item.posts}</Text>
                        <Text style={styles.contentPost}>{item.key}</Text>
                        <View style={styles.ViewLis}>
                        
                        <View style={styles.ViewLis}>
                        </View>

                    </View>
                        
         </View>          

                   
   
              
                )}
               
               />





         
          <View style={styles.containerBar}>
                    <View style={styles.main}>

                         <TouchableOpacity onPress={()=>this.props.navigation.navigate('Map')}>
                            <Icon name="ios-map" size={ 24}style={styles.icon} />
                        </TouchableOpacity>
                    </View>
            
                       <View style={styles.main}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('PostUser')}>
                            <Icon2 name="plus" size={ 24}style={styles.icon} />
                        </TouchableOpacity>
                      </View>
                      
                        <View style={styles.main}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('HomeVacina')}>
                            <Icon name="ios-notifications" size={ 24}style={styles.icon} />
                        </TouchableOpacity>
                        
                        </View>
            </View>
      </View>
    )
  }
}
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: '#fff',
       
      },
      text:{
        color:'black'
      },
      containerBar: {
        backgroundColor: '#fff',
        height: 44,
        paddingTop: 0,
        paddingHorizontal: 15,
        borderBottomWidth: 2,
        borderColor:'#111',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    main:{
        width:30,
        height:30,
        borderRadius: 15,
        backgroundColor:'#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon:{
        color: '#ff944d'
    },
    avatar:{
      width:34,
      height:34,
      borderRadius:34,
      //marginRight: 15,
      //marginTop:5,
},
repoNome:{

    fontSize: 15,
    fontWeight: "bold",
    color: "#1C2022",
    marginRight:150,

 
  

},

ViewLis:{
 //
  flexDirection: 'row',
 justifyContent: 'space-between',
  
},
repo:{
  padding: 20,
  backgroundColor: '#FFF',
  height:120,
  marginBottom: 5,
  borderRadius: 5,
  flexDirection: 'row',
},
repoInfo:{
  marginLeft: 5,
},
info:{
  marginLeft: 20,
  padding: 20
  
},
iconPosts:{ flexDirection: 'row', justifyContent: 'flex-end', padding: 20},

conteinerList:{
 
    padding: 20,
    borderBottomWidth: 1,
    borderColor: "#eee"
  },
  contentPost: {
    fontSize: 15,
    lineHeight: 20,
    color: "#1C2022",
    marginVertical: 10
  },

  


});
    

