import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'

export default class Index extends Component{
    static navigationOptions = { header: null }
  navegarLogin = () =>{
     this.props.navigation.navigate('Login')
  }
  navegarCadastro = () =>{
    this.props.navigation.navigate('CadastroLogin')
 }
 
 navegarLoginTest = () =>{
  this.props.navigation.navigate('LoginTest')
}
 
    render(){
        return(
            <View style={styles.container}>
            <View style={styles.content}>
                <Text style={styles.Texto}>BEM-VINDO </Text><Text style={styles.Texto}>SUS COLABORATIVO</Text>

                <TouchableOpacity style={styles.button} onPress={this.navegarLogin}>
                    <Text style={styles.buttonText}>Login</Text>

                    </TouchableOpacity>

                 <TouchableOpacity style={styles.button} onPress={this.navegarCadastro}>
                    <Text style={styles.buttonText}>Cadastre-se</Text>

                    </TouchableOpacity>


                  
                    </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#cccccc"
    },
    content: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: 30
      },
  
    content: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      padding: 30
    },

  
    button: {
      height: 44,
      alignSelf: "stretch",
      marginTop: 10,
      backgroundColor: "#ff944d",
      borderRadius: 5,
      justifyContent: "center",
      alignItems: "center"
    },
  
    buttonText: {
      color: "#FFF",
      fontSize: 16,
      fontWeight: "bold"
    },
     Texto:{   
        fontSize: 20,
        fontWeight: "bold"
    }
  });
  

