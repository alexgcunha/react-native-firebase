import React, { Component } from 'react';
import {View, Text, ActivityIndicator, StyleSheet, Button, BackHandler} from 'react-native';
import firebase from 'react-native-firebase';

export default class Escutar extends Component{
componentDidMount(){
  firebase.auth().onAuthStateChanged(user => {
    this.props.navigation.navigate(user ? 'Index' : 'Main')
 })
}
sair = () =>{
  BackHandler.exitApp();
}
render(){
    return(
        <View>
              <Button
            title="Deseja sair"
            onPress={this.sair}
        />
        </View>
    );
}
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    }
  })
    
