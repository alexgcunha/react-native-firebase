import React,{ Component } from 'react';
import {View, Text, StyleSheet,TextInput, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';

export default class CadastroLogin extends Component{
   // static navigationOptions = { header: null }

state = {
    
    email:"",
    pass:"",
    erroMessage: null 
};


registerLogin =  async () =>{
    const { email, pass } = this.state;
    try{
    await firebase.auth().createUserWithEmailAndPassword(email, pass)
	this.props.navigation.navigate('CadastroImage')
     
    }catch(error){
        console.log(error);
    }
 }


    render(){
        return(
            <View style={styles.container}>
                <View style={styles.content}>
                    <Text style={styles.Texto}>DADOS DO USUÁRIO</Text>
                    <TextInput 
                            style={styles.input}
                            placeholder="DIGITE SEU E-MAIL"
                            value={this.state.email}
                            onChangeText={email => this.setState({ email })}
                        
                        />
                    <TextInput 
                              secureTextEntry
                            style={styles.input}
                            placeholder="DIGITE SUA SENHA"
                            value={this.state.password}
                            onChangeText={pass => this.setState({ pass })}
                        
                        />
           
                        
                    <TouchableOpacity style={styles.button} onPress={this.registerLogin}>
                        <Text style={styles.buttonText}>Cadastrar</Text>

                        </TouchableOpacity>
                    </View>
                    
         </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#cccccc"
    },
  
    content: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      padding: 30
    },
  
    input: {
      borderWidth: 1,
      borderColor: "#333",
      borderRadius: 5,
      height: 44,
      paddingHorizontal: 15,
      alignSelf: "stretch",
      marginTop: 30
    },
    button: {
        height: 44,
        alignSelf: "stretch",
        marginTop: 10,
        backgroundColor: "#ff944d",
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center"
      },
    
      buttonText: {
        color: "#FFF",
        fontSize: 16,
        fontWeight: "bold"
      },
      Texto: {
          
          fontSize: 20,
          fontWeight: "bold"
      }
})