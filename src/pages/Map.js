import React, { Component } from 'react';

import { View, Text, StyleSheet, ScrollView, Dimensions } from 'react-native';

import  MapView from 'react-native-maps';



export default class Map extends Component {
  static navigationOptions = ({navigation})=>({
    title: "Map",
    
});

state = {
  
  places:[
    {
      id: 1,
      title: 'Posto de saúde paraíso dos passaros',
      
      latitude:-1.405047,
      longitude: -48.481590,
    },{
      id: 2,
      title: 'Unidade de Saúde da Sacramenenta',
      
      latitude:-1.413636 ,
      longitude: -48.468854,
    }
  ]
}
  render() {

    const { latitude, longitude}= this.state.places[0];
    return(
          
        <View style={styles.container}>

        <MapView
        ref={map => this.mapView = map}
          initialRegion={{
            latitude,
            longitude,
            latitudeDelta: 0.0142,
            longitudeDelta: 0.0131,
          }}
        
        style={styles.mapView}
        
              rotateEnabled={false}
              scrollEnabled={false}
              zoomEnabled={false}
              showsPointsOfInterest={false}
              showBuildings={false}
        >
        
          
         
            
          
        { this.state.places.map(place => (
          <MapView.Marker
          ref={mark => place.mark = mark}
          key={place.id}
          coordinate={{
            latitude: place.latitude ,
            longitude: place.longitude,
          }}
          />
        ))}
        </MapView>
        <ScrollView style={styles.placeContainer}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          onMomentumScrollEnd={e => {
            const scrolled = e.nativeEvent.contentOffset.x;

            const place  = (scrolled > 0) ? scrolled / Dimensions.get('window').width
            : 0;

            const { latitude, longitude} = this.state.places[place];

            this.mapView.animateToCoordinate({
              latitude,
              longitude,
            });
          }}
         
        > 
         { this.state.places.map(place => (
          <View key={place.id} style={styles.place}>
              <Text >{place.title}</Text>
      
          </View>
        ))}
        </ScrollView>

    </View>
    );
  }
}

const {height, width} = Dimensions.get('window')
 const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',

  },
  
  mapView: {
     position: "absolute",
     top: 0,
     left: 0,
     bottom: 0,
     right: 0,
   },
   placeContainer:{
     width:'100%',
     maxHeight:200,
   },
   place:{
    width: width - 40,
    maxHeight: 200,
    backgroundColor: '#FFF',
    marginHorizontal:20,
   }
 });