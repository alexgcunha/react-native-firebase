import React,{ Component } from 'react';
import {View, Text, StyleSheet,TextInput, TouchableOpacity, Image } from 'react-native';
import firebase from 'react-native-firebase';
import ImagePicker from 'react-native-image-picker';
export default class CadastroImage extends Component{
    static navigationOptions = { header: null }

state = {
    uid:'',
    photo: null
};

handleInputChange = () =>{
const options = {
    noData:true
};
    ImagePicker.launchImageLibrary(options, response =>{
        console.log('response', response);
        if(response.uri){
            this.setState({ photo: response })
        }
    });
};
cadastrar =  () =>{
    const userId = firebase.auth().currentUser.uid;
    const userEmail = firebase.auth().currentUser.email;
    const {   photo } = this.state;

    const imgaUrl = photo.uri;
     firebase.storage().ref().child(userEmail+'/' +  userId).put(photo.uri
            ).then(this.props.navigation.navigate('CadastroUser')
            )
 }




    render(){
        const { photo } = this.state;
        return(
            <View style={styles.container}>
           
                <View style={styles.content}>

 {photo && (<Image 
                source={{ uri: photo.uri }}
                style={{ width: 64, height: 64, borderRadius: 34 ,justifyContent:'center'  }}
            
            />)}



                    <Text style={styles.Texto}>Selecione Uma Imagem</Text>
                    
                    
                        <TouchableOpacity style={styles.button} onPress={this.handleInputChange}>
                        <Text style={styles.buttonText}>Imagem</Text>

                        </TouchableOpacity>

                            
                    <TouchableOpacity style={styles.button} onPress={this.cadastrar}>
                        <Text style={styles.buttonText}>Enviar</Text>

                        </TouchableOpacity>

                    </View>
         </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#cccccc"
    },
  
    content: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      padding: 30
    },
  
    input: {
      borderWidth: 1,
      borderColor: "#333",
      borderRadius: 5,
      height: 44,
      paddingHorizontal: 15,
      alignSelf: "stretch",
      marginTop: 30
    },
    button: {
        height: 44,
        alignSelf: "stretch",
        marginTop: 10,
        backgroundColor: "#ff944d",
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center"
      },
    
      buttonText: {
        color: "#FFF",
        fontSize: 16,
        fontWeight: "bold"
      },
      Texto: {
          
          fontSize: 20,
          fontWeight: "bold"
      }
})