import { createStackNavigator } from 'react-navigation';
import Login from './pages/Login';
import CadastroUser from './pages/CadastroUser'
import CadastroLogin from './pages/CadastroLogin'
import Escutar from './pages/Escutar';
import Index from './pages/Index';
import Main from './pages/Main';
import PostUser from './pages/PostUser';
import EsqueceuSenha from './pages/EsqueceuSenha';
import CadastroImage from './pages/CadastroImage';
import Map from './pages/Map';
import HomeVacina from './pages/HomeVacina';
import PostagemVacina from './pages/PostagemVacina';

const Routes = createStackNavigator({
  Escutar,
 Index, 
 Login,
  EsqueceuSenha,
  CadastroLogin,
  CadastroImage,
   CadastroUser,
  Main,  
 Map,   
  PostUser,
  HomeVacina,
  PostagemVacina,
});
export default Routes;