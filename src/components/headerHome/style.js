import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
container:{
    backgroundColor: '#ffffff',
    height:44,
    paddingTop: 0,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#EEE',
    flexDirection: 'row',
    justifyContent: 'space-between',
},
textHome:{
    marginTop: 10,
    fontWeight: 'bold',
    color: '#111',
    fontSize: 16,
},
icon:{
    color:'#111',
},
avatar:{
    width:34,
    height:34,
    borderRadius:34,
    marginRight: 15,
    marginTop:5,

},
});

export default styles;
