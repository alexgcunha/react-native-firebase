import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, BackHandler} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';
import styles from './style';



export default class HeaderHome extends Component{
    state = {
        imagem: null
    }
    componentDidMount(){
        const email = firebase.auth().currentUser.email;
        const ref = firebase.storage().ref()
               const uid = firebase.auth().currentUser.uid;
                      ref.child(email+ '/' + uid).getDownloadURL().then(url =>{
            
    
                          console.log(url)
                          this.setState({ imagem: url})
    
          }).then(this.handler).catch(function(error){
            console.log(error)
          })


    }


    sair = () =>{
        BackHandler.exitApp();
      }



    render(){
        const  { imagem } = this.state;

        return(
    <View style={styles.container}>
              <Image 
                style={styles.avatar}
                source={{uri : imagem}}
      />

                <Text style={styles.textHome}>Sus Colaborativo</Text>
                <TouchableOpacity onPress={this.sair}>
                        <Text style={styles.textHome}>Sair</Text>
                </TouchableOpacity>

    </View>
);
    }
}
